"use strict";

const box = document.getElementById("box");
const buttons = document.getElementsByTagName("button");
const circles = document.getElementsByClassName("circle");
const wrapper = document.querySelector(".wrapper");
const hearts = wrapper.querySelectorAll(".heart");
const oneHeart = document.querySelector(".heart");

box.style.backgroundColor = "green";
box.style.width = "500px";

box.style.cssText = "background-color: blue; height = 500px";

buttons[1].style.height = "3em";

circles[0].style.backgroundColor = "red";

hearts.forEach((item) => {
  item.style.backgroundColor = "blue";
});

const div = document.createElement("div");
const text = document.createTextNode("Какой-то текст на странице");

div.classList.add("black");

// wrapper.append(div);
wrapper.appendChild(div);

// wrapper.prepend(div);

// hearts[1].before(div);
wrapper.insertBefore(div, wrapper.children[1]);
// hearts[1].after(div);

// circles[1].remove();
document.body.removeChild(circles[1]);

// hearts[0].replaceWith(circles[0]);
wrapper.replaceChild(circles[0], hearts[1]);

div.innerHTML = "<h1>Hello world!</h1>";
// div.textContent = "<h1>Hello world!</h1>";

// div.insertAdjacentHTML("afterbegin", "<h2>Hello</h2>");
div.insertAdjacentText("afterbegin", "<h2>Hello</h2>");
